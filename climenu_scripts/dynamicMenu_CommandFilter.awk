BEGIN {
	menuCommandCount=0
}

{
	if ($1 == "menuItensCommand") {
		menuBuilderCommand=$2
		menuCommandCount++
	}
}

END {
	if (menuCommandCount == 1) {
		print menuBuilderCommand
	} else {
		print "Error: config file must have one \"menuItensCommand\" entry!"
	}
}
